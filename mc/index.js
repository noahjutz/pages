var headerApp = new Vue({
    el: "#header",
    data: {
        online: false
    }
})

async function getServerStatus() {
    const response = await fetch('https://api.mcsrvstat.us/2/mc.djutz.com');
    const myJson = await response.json(); //extract JSON from the http response
    headerApp.online = myJson.online
}
getServerStatus()


var modsApp = new Vue({
    el: "#mods",
    data: {
        modsDependency: [{
                name: "Cloth Config API",
                url: "https://www.curseforge.com/minecraft/mc-mods/cloth-config/files",
            },
            {
                name: "Cloth API",
                url: "https://www.curseforge.com/minecraft/mc-mods/cloth-api/files"
            },
            {
                name: "Architectury API",
                url: "https://www.curseforge.com/minecraft/mc-mods/architectury-fabric/files"
            },
            {
                name: "Fabric API",
                url: "https://www.curseforge.com/minecraft/mc-mods/fabric-api/files"
            }
        ],
        modsPerformance: [{
                name: "Starlight",
                url: "https://www.curseforge.com/minecraft/mc-mods/starlight/files"
            },
            {
                name: "Cull Leaves",
                url: "https://www.curseforge.com/minecraft/mc-mods/cull-leaves/files"
            },
            {
                name: "Dynamic FPS",
                url: "https://www.curseforge.com/minecraft/mc-mods/dynamic-fps/files"
            },
            {
                name: "LazyDFU",
                url: "https://www.curseforge.com/minecraft/mc-mods/lazydfu/files"
            },
            {
                name: "Entity Culling",
                url: "https://www.curseforge.com/minecraft/mc-mods/entityculling/files"
            }
        ],
        modsUtility: [{
                name: "Chest Tracker",
                url: "https://www.curseforge.com/minecraft/mc-mods/chest-tracker/files"
            },
            {
                name: "Diggus Maximus",
                url: "https://www.curseforge.com/minecraft/mc-mods/diggus-maximus/files"
            },
            {
                name: "Inventory Sorting",
                url: "https://www.curseforge.com/minecraft/mc-mods/inventory-sorting/files"
            },
            {
                name: "Ok Zoomer",
                url: "https://www.curseforge.com/minecraft/mc-mods/ok-zoomer/files"
            },
            {
                name: "Roughly Enough Items (REI)",
                url: "https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items/files"
            },
            {
                name: "AppleSkin",
                url: "https://www.curseforge.com/minecraft/mc-mods/appleskin/files"
            },
            {
                name: "Mod Menu",
                url: "https://www.curseforge.com/minecraft/mc-mods/modmenu/files"
            },
            {
                name: "Continuity",
                url: "https://www.curseforge.com/minecraft/mc-mods/continuity/files"
            }
        ]
    }
})