var app = new Vue({
    el: '#projects-container',
    data: {
        projects: [
            {
                name: "GymRoutines",
                description: "Workout tracker",
                source_url: "https://codeberg.org/noahjutz/GymRoutines",
                web_url: "gymroutines/index.html",
                icon: "assets/icons/gymroutines.svg"
            },
            {
                name: "Findchip",
                description: "Find location tags",
                source_url: "https://codeberg.org/noahjutz/Findchip",
                web_url: "findchip/index.html",
                icon: "assets/icons/findchip.svg"
            },
            {
                name: "Endict",
                description: "Dictionary of English and Translator",
                source_url: "https://codeberg.org/noahjutz/Endict",
                web_url: "endict/index.html",
                icon: "assets/icons/endict.svg"
            },
        ]
    }
});